﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace QualiumSystems.TestProject.StartProject
{
    public class StartProject : MonoBehaviour
    {
        public void GameStart()
        {
            SceneManager.LoadScene("Main");
        }
    }
}
