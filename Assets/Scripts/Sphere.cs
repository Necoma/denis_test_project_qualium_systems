﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QualiumSystems.TestProject.Menu;
using QualiumSystems.TestProject.Controlers;
using System;
namespace QualiumSystems.TestProject.PlayerObject
{
    public class Sphere : MonoBehaviour
    {
        private MenuControl menu;
        public void Init(MenuControl menu)
        {
            this.menu = menu;
        }
        private void OnTriggerExit(Collider colider)
        {
           menu.LoseGame();
        }
        
    }
}
