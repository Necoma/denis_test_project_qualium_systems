﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QualiumSystems.TestProject.Menu;
using QualiumSystems.TestProject.Controlers;
using QualiumSystems.TestProject.PlayerObject;



namespace QualiumSystems.TestProject.Control
{
    public class Initer : MonoBehaviour
    {
        [SerializeField]
        private MenuControl menuControl;
        [SerializeField]
        private Platform platform;
        [SerializeField]
        private Sphere sphere;


        private void Awake()
        {
            menuControl.Init(platform);
            platform.Init();
            sphere.Init(menuControl);
        }
     
    }
}