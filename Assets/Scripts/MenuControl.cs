﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using QualiumSystems.TestProject.PlayerObject;
using QualiumSystems.TestProject.Controlers;

namespace QualiumSystems.TestProject.Menu
{
    public class MenuControl : MonoBehaviour
    {
        [SerializeField]
        private GameObject panelMenu;
        [SerializeField]
        private Text timerText;
        

        private Platform platform;
       
        private float time;
        private float mSeconds;
        private float seconds;
        private float minutes;

        private const float milliSec = 100;
        private const float secTotal = 60;
        public void Init ( Platform platform)
        {
           
            this.platform = platform;
            Time.timeScale = 1;
            StartCoroutine(StopwatchTimer());
           
        }

        public void LoseGame()
        {
            Time.timeScale = 0;
            StopCoroutine(StopwatchTimer());
            panelMenu.SetActive(true);
        }


        public void GameRestart()
        {
            SceneManager.LoadScene((1));
        }
        public void ExitGame()
        {
            Application.Quit();
        }

        private IEnumerator StopwatchTimer()
        {
            {
                while (true)
                {
                    time += Time.deltaTime;
                    mSeconds = (int)((time - (int)time) * milliSec);
                    seconds = (int)(time % secTotal);
                    minutes = (int)(time / secTotal % secTotal);

                    timerText.text = string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, mSeconds);

                    yield return null;
                }
            }
        }
    }
}
