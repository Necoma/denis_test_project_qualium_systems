﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QualiumSystems.TestProject.Menu;

using System;

namespace QualiumSystems.TestProject.Controlers
{
    public class Platform : MonoBehaviour
    {
       
        public float sensetivityHor = 9.0f;
        public float sensetiviryVert = 9.0f;
        private float rotationX = 0;

        private const string mouseX = "Mouse X";
        private const string mouseY = "Mouse Y";

        public void Init()
        {
            StartCoroutine(RotatPlatform());
        }
         private IEnumerator RotatPlatform ()
        {
            
            while (true)
            {
                
                    float pointerX = Input.GetAxis(mouseX);
                    float pointerZ = Input.GetAxis(mouseY);
                    if (Input.touchCount > 0)
                    {
                        pointerX -= Input.touches[0].deltaPosition.x;
                        pointerZ -= Input.touches[0].deltaPosition.y;
                    }
                    rotationX -= pointerZ * sensetiviryVert * Time.deltaTime;
                    float delta = pointerX * sensetivityHor * Time.deltaTime;
                    float rotationZ = transform.localEulerAngles.z + delta;

                    transform.localEulerAngles = new Vector3(rotationX, 0, rotationZ);
                    
                
                yield return null;
            }
        }
    
    }
}
